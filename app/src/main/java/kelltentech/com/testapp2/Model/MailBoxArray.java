package kelltentech.com.testapp2.Model;

import java.util.ArrayList;

/**
 * Created by dheeraj.kandpal on 09-09-2016.
 */
public class MailBoxArray {

    public ArrayList<MailOutBox> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<MailOutBox> messages) {
        this.messages = messages;
    }

    ArrayList<MailOutBox> messages;
}
