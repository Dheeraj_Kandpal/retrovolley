package kelltentech.com.testapp2.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dheeraj.kandpal on 13-09-2016.
 */
public class Base2Style {

//    login": "basil2style",
//            "id": 1285344,
//            "avatar_url": "https://avatars.githubusercontent.com/u/1285344?v=3",
//            "gravatar_id": "",
//            "url": "https://api.github.com/users/basil2style",
//            "html_url": "https://github.com/basil2style",
//            "followers_url": "https://api.github.com/users/basil2style/followers",
//            "following_url": "https://api.github.com/users/basil2style/following{/other_user}",
//            "gists_url": "https://api.github.com/users/basil2style/gists{/gist_id}",
//            "starred_url": "https://api.github.com/users/basil2style/starred{/owner}{/repo}",
//            "subscriptions_url": "https://api.github.com/users/basil2style/subscriptions",
//            "organizations_url": "https://api.github.com/users/basil2style/orgs",
//            "repos_url": "https://api.github.com/users/basil2style/repos",
//            "events_url": "https://api.github.com/users/basil2style/events{/privacy}",
//            "received_events_url": "https://api.github.com/users/basil2style/received_events",
//            "type": "User",
//            "site_admin": false,
//            "name": "Basil",
//            "company": "MakeInfo",
//            "blog": "http://www.themakeinfo.com",
//            "location": "Toronto,Canada",
//            "email": "basiltalias92@gmail.com",
//            "hireable": true,
//            "bio": null,
//            "public_repos": 38,
//            "public_gists": 4,
//            "followers": 59,
//            "following": 152,
//            "created_at": "2011-12-26T00:17:22Z",
//            "updated_at": "2016-09-13T00:54:15Z"

    @SerializedName("basil2style")
    String login;

    @SerializedName("id")
    String id;

    @SerializedName("avatar_url")
    String avtar_url;

    @SerializedName("gravatar_id")
    String gravtar_id;

    @SerializedName("url")
    String url;

    @SerializedName("html_url")
    String html_url;

    @SerializedName("followers_url")
    String followers_url;

    @SerializedName("following_url")
    String following_url;

    @SerializedName("gists_url")
    String gists_url;

    @SerializedName("starred_url")
    String starred_url;
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvtar_url() {
        return avtar_url;
    }

    public void setAvtar_url(String avtar_url) {
        this.avtar_url = avtar_url;
    }

    public String getGravtar_id() {
        return gravtar_id;
    }

    public void setGravtar_id(String gravtar_id) {
        this.gravtar_id = gravtar_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getFollowers_url() {
        return followers_url;
    }

    public void setFollowers_url(String followers_url) {
        this.followers_url = followers_url;
    }

    public String getFollowing_url() {
        return following_url;
    }

    public void setFollowing_url(String following_url) {
        this.following_url = following_url;
    }

    public String getGists_url() {
        return gists_url;
    }

    public void setGists_url(String gists_url) {
        this.gists_url = gists_url;
    }

    public String getStarred_url() {
        return starred_url;
    }

    public void setStarred_url(String starred_url) {
        this.starred_url = starred_url;
    }


}
