package kelltentech.com.testapp2.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kelltentech.com.testapp2.MainActivity;
import kelltentech.com.testapp2.Model.MailBoxArray;
import kelltentech.com.testapp2.Model.MailOutBox;
import kelltentech.com.testapp2.R;

/**
 * Created by dheeraj.kandpal on 09-09-2016.
 */
public class ListAdapter extends BaseAdapter implements View.OnClickListener{

    /*********** Declare Used Variables *********/
    private Context activity;
    private MailBoxArray data;
    private static LayoutInflater inflater=null;
    public Resources res;
    MailOutBox tempValues=null;
    int i=0;
    private int mPosition;
    /*************  ListAdapter Constructor *****************/
    public ListAdapter(Context a, MailBoxArray d) {

        /********** Take passed values **********/
        activity = a;
        data=d;


        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(data.getMessages().size()<=0)
            return 1;
        return data.getMessages().size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView textViewid;
        public TextView textViewto;
        public TextView textViewsubject;
        public TextView textViewmessage;
        public TextView textViewDate;


    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.custom_listview, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.textViewid = (TextView) vi.findViewById(R.id.tvid);
            holder.textViewsubject=(TextView)vi.findViewById(R.id.tvsubject);
            holder.textViewto=(TextView)vi.findViewById(R.id.tvto);
            holder.textViewDate=(TextView)vi.findViewById(R.id.tvdate);
            holder.textViewmessage=(TextView)vi.findViewById(R.id.tvmessage);
            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.getMessages().size()<=0)
        {
            holder.textViewmessage.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues = ( MailOutBox ) data.getMessages().get( position );

            /************  Set Model values in Holder elements ***********/


                holder.textViewid.setText((Html.fromHtml("<b>"+"ID:-"+"</b>")+tempValues.getId().toUpperCase()));
                holder.textViewto.setText((Html.fromHtml("<b>"+"TO:-"+"</b>")+ tempValues.getTo()).toUpperCase() );
                holder.textViewDate.setText( (Html.fromHtml("<b>"+"DATE:-"+"</b>")+tempValues.getDate()).toUpperCase() );
                holder.textViewmessage.setText((Html.fromHtml("<b>"+"MESSAGE:-"+ "</b>")+tempValues.getMessage()).toUpperCase() );
                holder.textViewsubject.setText((Html.fromHtml("<b>"+"SUBJECT:-"+ "</b>")+tempValues.getSubject()).toUpperCase() );

            holder.textViewid.setTypeface(null, Typeface.BOLD);
            holder.textViewto.setTypeface(null, Typeface.BOLD);
            holder.textViewDate.setTypeface(null, Typeface.BOLD);
            holder.textViewmessage.setTypeface(null, Typeface.BOLD);
            holder.textViewsubject.setTypeface(null, Typeface.BOLD);



            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new OnItemClickListener( position ));
        }
        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("ListAdapter", "=====Row button clicked=====");
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener {


        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {


            Toast.makeText(activity,""+mPosition,Toast.LENGTH_SHORT).show();
        }
    }

}
