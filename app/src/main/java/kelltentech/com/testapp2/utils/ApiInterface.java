package kelltentech.com.testapp2.utils;

import kelltentech.com.testapp2.Model.Base2Style;
import kelltentech.com.testapp2.Model.MoviesResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by dheeraj.kandpal on 12-09-2016.
 */
public interface ApiInterface {
//https://api.themoviedb.org/3/discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc&api_key=1cc6d39fe7aefa4320c902ee929da8d9

   // https://api.themoviedb.org/3/discover/movie?with_genres=18&primary_release_year=2018&api_key=1cc6d39fe7aefa4320c902ee929da8d9
    @GET("movie/top_rated")
    Call<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<MoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);

    @GET("discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc")
    Call<MoviesResponse> getPopularMovies(@Query("api_key") String apiKey);

    @FormUrlEncoded
    @POST("discover/movie?with_genres=18")
    Call<MoviesResponse>getMovieByYear(@Field("primary_release_year") int year, @Query("api_key") String apiKey);

 @GET("users/basil2style")
 Call<Base2Style> getLoginDetails();


}

