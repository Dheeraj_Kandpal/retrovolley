package kelltentech.com.testapp2.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dheeraj.kandpal on 12-09-2016.
 */
public class ApiClient {
//image url=http://image.tmdb.org/t/p/w185//5N20rQURev5CNDcMjHVUZhpoCNC.jpg

//hit url=http://api.themoviedb.org/3/discover/movie?api_key=1cc6d39fe7aefa4320c902ee929da8d9

// https://api.themoviedb.org/3/discover/movie?primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22&api_key=1cc6d39fe7aefa4320c902ee929da8d9

    public static final String BASE_URL = "http://api.themoviedb.org/3/";
    public static final String BASE_URL1 = "https://api.github.com/";
    public static final String IMAGE_URL="http://image.tmdb.org/t/p/w185/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient(String base_url) {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
