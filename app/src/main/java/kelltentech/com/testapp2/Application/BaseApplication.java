package kelltentech.com.testapp2.Application;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.BuildConfig;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by dheeraj.kandpal on 09-09-2016.
 */
public class BaseApplication extends Application{

    private final String LOG_TAG = "BaseApplication";

    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;
    public boolean mAppInBackground;
    private final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;


    private String FILE_DIR = ".assist_keystore";
    private String cardCode;
    private SQLiteDatabase mWritableDatabase;
    public Boolean isCheckNetwork = false;
    private RequestQueue mRequestQueue;

    //
    private static BaseApplication mInstance;
    /*
     * Google Analytics configuration values.

    // private static final String GA_PROPERTY_ID = "UA-55320061-1"; old account
    // dev
    // private static final String GA_PROPERTY_ID = "UA-57703765-1"; new account
    // jonu
    // private static final String GA_PROPERTY_ID = "UA-58849015-1"; // new
    // account muditoneassist

    // Dispatch period in seconds.
    private static final int GA_DISPATCH_PERIOD = 5;

    // Prevent hits from being sent to reports, i.e. during testing.
    // private static final boolean GA_IS_DRY_RUN = false;

    // GA Logger.
    // private static final LogLevel GA_LOG_VERBOSITY = LogLevel.VERBOSE;

    // Key used to store a user's tracking preferences in SharedPreferences.
    private static final String TRACKING_PREF_KEY = "trackingPreference";

	/*
     * Method to handle basic Google Analytics initialization. This call will
	 * not block as all Google Analytics work occurs off the main thread.
	 */

    public static final String TAG = BaseApplication.class.getSimpleName();


    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getCardCode() {
        return this.cardCode;
    }

    /*
     * Returns the Google Analytics tracker.
     */
	/*
	 * public static com.google.analytics.tracking.android.Tracker
	 * getGaTracker() { return mTracker; }
	 */

	/*
	 * Returns the Google Analytics instance.
	 */
	/*
	 * public static com.google.analytics.tracking.android.GoogleAnalytics
	 * getGaInstance() { return mGa; }
	 */



    public static Context getAppContext() {
        return getInstance();
    }

    public static BaseApplication getInstance() {
        return mInstance;
    }

    protected static void setInstance(BaseApplication mInstance) {
        BaseApplication.mInstance = mInstance;
    }

    //
	/*
	 * (non-Javadoc)
	 *
	 * @see android.app.Application#onCreate()
	 */
    @Override
    public void onCreate() {
        super.onCreate();
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        setInstance(this);
        printHashKey();


    }
    public void printHashKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "kelltentech.com.testapp2",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }/*
		 * FontsOverride.setDefaultFont(this, "DEFAULT",
		 * "myriad_pro_regular.ttf"); FontsOverride.setDefaultFont(this,
		 * "MONOSPACE", "myriad_pro_regular.ttf");
		 * FontsOverride.setDefaultFont(this, "SERIF",
		 * "myriad_pro_regular.ttf"); FontsOverride.setDefaultFont(this,
		 * "SANS_SERIF", "SignPainter-HouseScript.ttf");

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.d("push service", "push service is running");
                return true;
            }
        }
        Log.d("push service", "push service is not running");
        return false;
    }

    /**

    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Application#onTerminate()
     */


    /**
     * @return the mIsAppInBackground
     */
    public boolean isAppInBackground() {
        return mAppInBackground;
    }

    /**
     * @param isAppInBackground value to set for mIsAppInBackground
     */
    public void setAppInBackground(boolean isAppInBackground) {
        mAppInBackground = isAppInBackground;
        onAppStateSwitched(isAppInBackground);
    }

    /**
     * @param isAppInBackground
     * @note subclass can override this method for this callback
     */
    protected void onAppStateSwitched(boolean isAppInBackground) {
        // nothing to do here in base application class
    }

    /**
     * should be called from onResume of each activity of application
     */
    public void onActivityResumed() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }
        setAppInBackground(true);
    }

    /**
     * should be called from onPause of each activity of app
     */
    public void onActivityPaused() {
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                setAppInBackground(false);
                if (BuildConfig.DEBUG) {
                    Log.i(LOG_TAG, "None of our activity is in foreground.");
                }
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask, MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
