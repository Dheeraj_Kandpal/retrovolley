package kelltentech.com.testapp2.CustomUi;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by dheeraj.kandpal on 14-09-2016.
 */
public class CustomTextView extends TextView {
    public static Typeface FONT_NAME;

    public CustomTextView(Context context) {
        super(context);
        if(FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto_BoldItalic.ttf");
        this.setTypeface(FONT_NAME);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto_BoldItalic.ttf");
        this.setTypeface(FONT_NAME);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto_BoldItalic.ttf");
        this.setTypeface(FONT_NAME);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto_BoldItalic.ttf");
        this.setTypeface(FONT_NAME);
    }
    
}
