package kelltentech.com.testapp2;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import kelltentech.com.testapp2.Model.Base2Style;
import kelltentech.com.testapp2.Model.Movie;
import kelltentech.com.testapp2.Model.MoviesResponse;
import kelltentech.com.testapp2.adapter.MoviesAdapter;
import kelltentech.com.testapp2.utils.ApiClient;
import kelltentech.com.testapp2.utils.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MovieByYear#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MovieByYear extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ImageView imageView=null;
ProgressBar progressBar=null;
    private final static String API_KEY = "1cc6d39fe7aefa4320c902ee929da8d9";

    public MovieByYear() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MovieByYear.
     */
    // TODO: Rename and change types and number of parameters
    public static MovieByYear newInstance(String param1, String param2) {
        MovieByYear fragment = new MovieByYear();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_by_year, container, false);
        final RecyclerView recyclerView = (RecyclerView)rootView. findViewById(R.id.movies_recycler_view);
        imageView=(ImageView) rootView.findViewById(R.id.imageView);
        progressBar=(ProgressBar) rootView.findViewById(R.id.progressBar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        if(!MainActivity.yearSelected.equalsIgnoreCase("")) {
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            ApiInterface apiService = ApiClient.getClient(ApiClient.BASE_URL).create(ApiInterface.class);

            Call<MoviesResponse> call = apiService.getMovieByYear(Integer.parseInt(MainActivity.yearSelected),API_KEY);
            call.enqueue(new Callback<MoviesResponse>() {
                @Override
                public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                    List<Movie> movies = response.body().getResults();
                    recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getActivity()));
                    Picasso.with(getContext()).load("http://image.tmdb.org/t/p/w185//5N20rQURev5CNDcMjHVUZhpoCNC.jpg").resize(100,100).into(imageView);
                    pDialog.hide();
                    progressBar.setVisibility(View.INVISIBLE);

                    Log.d("MovieByYear", "Number of movies received: " + movies.size());
                }

                @Override
                public void onFailure(Call<MoviesResponse> call, Throwable t) {
                    // Log error here since request failed
                    Log.e("MovieByYear", t.toString());

                    pDialog.hide();
                }
            });
        }
        else
            Toast.makeText(getActivity(),"Please Select the Year",Toast.LENGTH_SHORT).show();
//        ApiInterface apiInterface=ApiClient.getClient(ApiClient.BASE_URL1).create(ApiInterface.class);
//        Call<Base2Style> call=apiInterface.getLoginDetails();
//call.enqueue(new Callback<Base2Style>() {
//    @Override
//    public void onResponse(Call<Base2Style> call, Response<Base2Style> response) {
//        Base2Style base2Style=response.body();
//    }
//
//    @Override
//    public void onFailure(Call<Base2Style> call, Throwable t) {
//
//    }
//});

        // Inflate the layout for this fragment
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
